
## iLMS TA Batch grades upload

### Usage

1. Get the course ID and HW ID from url of homework page
    - Here is an example of DSP Homework 1
    - EX: https://lms.nthu.edu.tw/course.php?courseID=38139&f=hw_doclist&hw=183123
        - Course ID: 38139
        - HW ID: 183123
2. Replace the values in ta_info.json with the IDs in step 1 and your account and password on iLms

3. Create a csv of the grades and comments for each student
    - format: id, score, comments
    - Replace the values in ta_info.json with the filename of your csv
    
4. Execute ilms.py to set the score and comment for this homework

### Execution Flow

1. loads the ta's information
2. Download the student's information from iLMS.

    - the submit ID varies with the status of the homework(submitted, paper...)
    
3. Link student's information with the score

    - the link is established with student's ID
    
4. Set score and comment.

### Todo

- [x] Use a single session for the whole process
- [ ] Better class decoupling for TA and students
- [ ] Add the flow of the group HW
- [ ] A CLI router for different purposes
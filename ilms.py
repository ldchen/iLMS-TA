#%%
import requests
from bs4 import BeautifulSoup
import csv
import json
import imageio
import matplotlib.pyplot as plt
import re
import shutil
import pathlib
import io
#%%
class TA:
    BASE_URL = 'http://lms.nthu.edu.tw'
    GRADE_PAGE = f'{BASE_URL}/course/hw_score.php'
    PAPER_GRADE_PAGE = f'{BASE_URL}/course/hw_paper_score.php'
    REF_PAGE = f'{BASE_URL}/course/hw_score.php'
    LOGIN_PAGE = 'https://lms.nthu.edu.tw/login_page.php?ssl=1&from=%2Fhome.php'
    LOGIN_SUBMIT = f'{BASE_URL}/sys/lib/ajax/login_submit.php'
    EXT_HEADER = {'Referer': 'https://lms.nthu.edu.tw/course/hw_score.php'}

    def __init__(self, info_file):
        with open(info_file) as f:
            data = json.load(f)
            self.SCORE_FILE = data['Score_File']
            self.COURSE_ID = data['COURSE_ID']
            self.HW_ID = data['HW_ID']
            self.LOGIN_INFO = dict(account=data['account'], password=data['password'], stay='1')
        self.HW_DOC_LIST = f'{self.BASE_URL}/course.php?courseID={self.COURSE_ID}&f=hw&hw={self.HW_ID}'
        self.NO_HW = f'http://lms.nthu.edu.tw/course.php?courseID={self.COURSE_ID}&f=nohwlist&hw={self.HW_ID}'
        self.HW = f'http://lms.nthu.edu.tw/course.php?courseID={self.COURSE_ID}&f=hw_doclist&hw={self.HW_ID}'
        self.session = requests.session() 
    
    def login(self):
        for _ in range(20):
            # Try to login 
            my_headers ={
                'Connection': 'keep-alive',
                'Host': 'lms.nthu.edu.tw',
                'Sec-Fetch-Site': 'same-origin',
                'Sec-Fetch-Mode': 'navigate',
                'Sec-Fetch-User': '?1',
                'Sec-Fetch-Dest': 'document',
                'Referer': 'https://lms.nthu.edu.tw/home.php'
                }
            resp = self.session.get(self.LOGIN_PAGE, headers=my_headers)
            resp.encoding='utf-8'
            bs = BeautifulSoup(resp.text, "lxml")
            rows = bs.findChildren('img') # get submitted student table
            sec = self.BASE_URL+str(rows[1]).split('"')[3].replace('amp;', '')
            my_headers = {
                'accept':
                'image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8',
                'accept-encoding':'gzip, deflate, br',
                'accept-language':'en-US,en;q=0.9',
                'connection':'keep-alive',
                'host':'lms.nthu.edu.tw',
                'referer':'https://lms.nthu.edu.tw/login_page.php?ssl=1&from=%2Fhome.php',
                'sec-fetch-dest':'image',
                'sec-fetch-mode':'no-cors',
                'sec-fetch-site':'same-origin',
                }
            resp = self.session.get(sec, headers=my_headers)
            image_bytes = io.BytesIO(resp.content)
            img = imageio.imread(image_bytes)
            # print_capcha(img)
            verify_code = ocr(img)
            # print(verify_code)
            # verify_code = input("Enter verification code\n")
            my_headers ={
                'Host': 'lms.nthu.edu.tw',
                'Connection': 'keep-alive',
                'Origin': 'https://lms.nthu.edu.tw',
                'Sec-Fetch-Site': 'same-origin',
                'Sec-Fetch-Mode': 'cors',
                'Sec-Fetch-Dest': 'empty',
                'Referer': 'https://lms.nthu.edu.tw/login_page.php?ssl=1&from=%2Fhome.php'
            }
            self.LOGIN_INFO['secCode'] = verify_code
            resp = self.session.post(self.LOGIN_SUBMIT, data=self.LOGIN_INFO, headers=my_headers)
            resp = json.loads(resp.text)
            # print()
            # print(resp['status'])
            if resp['ret']['status'] == 'true':
                print('Login Success')
                return
            else:
                print('Failed')
                # print_capcha(img)
                # print(verify_code)
        print('Login Failed, Something went wrong. Ask LiDe for support')
            # print(resp.text)
        # return img
#%%

def argmin(li):
    tmp = li[0]
    idx = 0
    for i in range(1, len(li)):
        if li[i]<tmp:
            idx = i
            tmp = li[i]
    return idx

def ocr(img):
    img = 255 - img[2:-5, 17:-15, 2]
    img_list = [imageio.imread(f'pattern/{i}.png') for i in range(1,10)]
    img[img>128] = 255
    img[img<128] = 0
    result = []
    for i in range(4):
        # verify using argmin sad(img, pat[i])
        diff = []
        for t in img_list:
            cor = (abs(img[:, i*12:(i+1)*12]-t)).sum()
            diff.append(cor)
        am = argmin(diff)+1
        result.append(am)
    res = 0
    for r in result:
        res = res*10+r
    return res

def print_capcha(img):
    img = 255 - img[2:-5, 17:-15, 2]
    for h in range(img.shape[0]):
        for w in range(img.shape[1]):
            if img[h, w] > 128:
                print('@', end='')
            else:
                print(' ', end='')
        print('')

def get_nohw_students(ta):
    """ get student in a list """
    students_dict = {}
    # with requests.session() as session:
    # -- login
    ta.session.post(ta.LOGIN_PAGE, data=ta.LOGIN_INFO)
    # -- get submitted hw page
    resp = ta.session.get(ta.NO_HW)
    resp.encoding = 'utf-8'
    submitted_soup = BeautifulSoup(resp.text, "lxml")
    # -- get student list
    rows = submitted_soup.findChildren('tr') # get submitted student table
    for row in rows[1:]:
        items = row.findChildren('td')
        if len(items) > 6:
            # name = items[2].text
            sid = items[1].text
            submit_id = items[7].find('a', href=True)['href'].split('(')[1][:-1]
            students_dict[sid] = [sid, submit_id, 0]
    return students_dict


def get_submitted_student(ta):
    """ get student in a list """
    students = {}
    resp = ta.session.get(ta.HW)
    resp.encoding = 'utf-8'
    print(resp.status_code)
    with open('a.html', 'w') as f:
        f.write(resp.text)
    submitted_soup = BeautifulSoup(resp.text, "lxml")
    # -- get student list
    rows = submitted_soup.findChildren('tr') # get submitted student table
    for row in rows[1:]:
        items = row.findChildren('td')
        if len(items) > 6:
            # name = items[3].text
            sid = items[2].text
            if len(sid)> 7:
                submit_id = items[6].findChildren('span')[0].get('id').replace('status', '')
                students[sid] = [sid, submit_id, 1]
    return students


def set_score_comment(stu, ta):
    my_headers ={
        'Connection': 'keep-alive',
        'Host': 'lms.nthu.edu.tw',
        'Origin': 'https://lms.nthu.edu.tw',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-Mode': 'navigate',
        'Sec-Fetch-User': '?1',
        'Sec-Fetch-Dest': 'iframe',
        'Referer': f'https://lms.nthu.edu.tw/course/hw_score.php?id={stu[1]}'
        }
    if stu[2] > 0:
        g_data = {
            'id': stu[1],
            'fmSubmit': 'yes',
            'fmStatus': '1',
            'fmScore': stu[3],
            'fmScoreNote': stu[4]
        }
        resp = ta.session.post(ta.GRADE_PAGE, data=g_data, headers=my_headers)
    else:
        g_data = {
            'fmSubmit': 'yes',
            'fmStatus': 1,
            'hw': ta.HW_ID,
            'userID': stu[1],
            'fmScore': stu[3],
            'fmNote': stu[4]
        }
        header = {
            'Referer': f'{ta.PAPER_GRADE_PAGE}?hw={ta.HW_ID}&id={stu[1]}'
        }
        resp = ta.session.post(ta.PAPER_GRADE_PAGE, data=g_data, headers=header)

    return resp


def gather_student_submitted(ta):
    no_hw_students = get_nohw_students(ta)
    has_hw_students = get_submitted_student(ta)
    return {**no_hw_students, **has_hw_students}


def link_student_score(student_dict, score_list):
    for score in score_list:
        if score[0] in student_dict:
            student_dict[score[0]].append(score[1])
            student_dict[score[0]].append(score[2])
        else:
            # print(type(score[0]), type(student_dict[0]))
            print(score[0], "Not in submitted student list")
    for s in student_dict:
        if len(student_dict[s]) < 4:
            student_dict[s].append('0')
            student_dict[s].append('not submitted')


def get_score_list(filename):
    with open(filename, newline='') as csv_file:
        rows = csv.reader(csv_file)
        score_list = list(rows)
    final = []
    for s in score_list[1:]:
        cmt = ''
        for q, qs in zip(score_list[0][2:], s[2:]):
            cmt += f'{q}: {qs} \n'
        # cmt += 'Test Version\n'
        cmt += '== Grading Finished'
        final.append([s[0], s[1], cmt])
    return final


def submit(ta, graded_student):
    for stu in graded_student.values():
        resp = set_score_comment(stu, ta)
        print(stu, resp)


def submit_view_only(graded_student):
    for stu in graded_student.values():
        print(stu)


def download(stu_list, destination='downloaded'):
    for s in stu_list:
        cid = stu_list[s][1]
        hw_page = f'https://lms.nthu.edu.tw/course.php?courseID={ta.COURSE_ID}&f=doc&cid={cid}'
        print(f'Download Files for {s}')
        # print(hw_page)
        resp = ta.session.get(hw_page)
        resp.encoding = 'utf-8'
        soup = BeautifulSoup(resp.text, "lxml")
        result = soup.findAll('a')
        id = []
        pathlib.Path(destination+f'/{s}').mkdir(parents=True, exist_ok=True)
        for r in result:
            link = r.get('href')
            filename = r.text
            if link[:16]=='/sys/read_attach':
                tmp = link.split('=')[1]
                if tmp not in id:
                    id.append(tmp)
                else: # Download
                    resp = ta.session.get(ta.BASE_URL+link)
                    with open(destination+f'/{s}/'+filename, 'wb') as f:
                        f.write(resp.content)
                    print(filename)
        print('=======Finished======')

def generate_stucsv(stu_list, file='stu_list.csv'):
    with open(file, 'w') as f:
        for s in stu_list:
            f.writelines(s+",")
#%%
if __name__ == "__main__":
    # Load TA information
    ta = TA('ta_info.json')
    ta.login()
    stu_list = get_submitted_student(ta)
    # generate_stucsv(stu_list)
    # Download to Destination folder
    # download(stu_list, destination='HW1')
    scores = get_score_list(ta.SCORE_FILE)
    link_student_score(stu_list, scores)

    # Submit Grading result with comments
    # view only print result
    # submit_view_only(stu_list)
    submit(ta, stu_list)
